const colors = ["green", "red", "rgba(133,122,200)", "pink"];
const btn = document.getElementById("btn");
const color = document.querySelector(".color");

// each time btn is clicked bg color changes and text in
btn.addEventListener("click",function(){
  //console.log(document.body);
  // get random number between colors[0] and colors[3]
  const randomNumber = getRandomNumber();
  console.log(randomNumber);
  // change bg color
  document.body.style.backgroundColor = colors[randomNumber];
  // change color text
  color.textContent = colors[randomNumber];
});

function getRandomNumber(){
  // math.random gives a random number between 0 and 0.99 * array length
  // and math.floor to round the random numbers
  return Math.floor(Math.random() * colors.length);
}
